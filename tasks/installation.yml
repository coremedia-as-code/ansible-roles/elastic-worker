---

- include_tasks: install_tomcat.yml

- name: create common and server lib directories
  file:
    path: "{{ elastic_worker.user_home }}/{{ item }}"
    state: directory
    owner: "{{ elastic_worker_user }}"
    group: "{{ coremedia_base_group }}"
    mode: 0750
  loop:
    - server-lib
    - common-lib

- name: propagate coremedia custom jar
  copy:
    src: "{{ deployment_tmp_directory }}/coremedia-tomcat.jar"
    dest: "{{ elastic_worker.user_home }}/common-lib"
    owner: "{{ elastic_worker_user }}"
    group: "{{ coremedia_base_group }}"
    remote_src: true

- name: propagate catalina-jmx-remote.jar
  copy:
    src: "{{ deployment_tmp_directory }}/catalina-jmx-remote.jar"
    dest: "{{ elastic_worker.user_home }}/current/lib/"
    owner: "{{ elastic_worker_user }}"
    group: "{{ coremedia_base_group }}"
    remote_src: true

- name: create application directory
  file:
    path: "{{ elastic_worker.user_home }}/current/webapps/{{ coremedia_application_name }}"
    state: directory
    owner: "{{ elastic_worker_user }}"
    group: "{{ coremedia_base_group }}"
    mode: 0750

- block:

    - name: download application war archiv {{ artefact_name }} to local directory {{ local_tmp_directory }}
      become: false
      get_url:
        url: "{{ artefact_url }}/{{ artefact_name }}"
        dest: "{{ local_tmp_directory }}"
        checksum: "sha1:{{ artefact_checksum }}"
      register: _download_artefact
      until: _download_artefact is succeeded
      retries: 5
      delay: 2
      run_once: true
      check_mode: false
      delegate_to: localhost

  when: artefact_download_needed


- block:

    - name: deploy application archiv
      copy:
        src: "{{ local_tmp_directory }}/{{ artefact_name }}"
        dest: "{{ deployment_tmp_directory }}"
        checksum: "{{ artefact_checksum }}"

    - name: unpack archive
      unarchive:
        src: "{{ deployment_tmp_directory }}/{{ artefact_name }}"
        dest: "{{ elastic_worker.user_home }}/current/webapps/{{ coremedia_application_name }}"
        owner: "{{ elastic_worker_user }}"
        group: "{{ coremedia_base_group }}"
        mode: 0750
        copy: false

    - name: get system timestamp  # noqa 305
      shell: 'date +"%Y-%m-%d %H-%M-%S %Z"'
      register: timestamp
      no_log: true
      tags:
        - skip_ansible_lint

    - name: set facts
      set_fact:
        current_date: "{{ timestamp.stdout[0:10] }}"
        current_time: "{{ timestamp.stdout[11:] }}"
        current_timestamp: "{{ timestamp.stdout }}"
        application_checksum: "{{ artefact_checksum }}"
      no_log: true

    - name: create custom fact file
      template:
        src: application.fact.j2
        dest: "/etc/ansible/facts.d/{{ coremedia_application_name }}.fact"
        owner: 'root'
        group: 'root'
        mode: '0750'

    - name: do facts module to get latest information
      setup:

  when: ( artefact_download_needed or
    (_deployed_application.stat is defined and not _deployed_application.stat.exists) )

- name: create systemd service unit
  template:
    src: "init/systemd/application.service.j2"
    dest: "{{ systemd_lib_directory }}/{{ coremedia_application_name }}.service"
    owner: root
    group: root
    mode: 0644
