# CoreMedia - elastic-worker

## Ports

```
40605
40699
40698
40680
40609
```

## dependent services

### solr
```
  elastic:
    social:
      solr:
        url: http://127.0.0.1:40080/solr
```

### content-management-server
```
elastic_worker:
  repository:
    url: http://content-management-server.int:40180/content-management-server/ior
```
